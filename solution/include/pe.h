/// @file
/// @brief File that describes the structure of a PE file


#ifndef SECTION_EXTRACTOR_PE_H
#define SECTION_EXTRACTOR_PE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>


#define MAGIC_VALUE 0x00004550
#define OFFSET_TO_PE_SIGNATURE 0x3c

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif
struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
/// At the beginning of an object file, or immediately after the signature of an image file,
/// is a standard COFF file header in the following format.
/// Note that the Windows loader limits the number of sections to 96.
PEHeader {
    /// The number that identifies the type of target machine
    uint16_t machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t number_of_sections;
    /// The low 32 bits of the number of seconds since 00:00 January 1,1970(a C run-time time_t value),
    /// which indicates when the file was created
    uint32_t time_date_stamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
    /// This value should be zero for an image because COFF debugging information is deprecated
    uint32_t pointer_to_symbol_table;
    /// The number of entries in the symbol table.
    /// This data can be used to locate the string table, which immediately follows the symbol table.
    /// This value should be zero for an image because COFF debugging information is deprecated
    uint32_t number_of_symbols;
    /// The size of the optional header, which is required for executable files but not for object files.
    /// This value should be zero for an object file.
    uint16_t size_of_optional_header;
    /// The flags that indicate the attributes of the file
    uint16_t characteristics;
};

struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
SectionHeader {
    /// An 8-byte, null-padded UTF-8 encoded string.
    /// If the string is exactly 8 characters long, there is no terminating null.
    /// For longer names, this field contains a slash (/)
    /// that is followed by an ASCII representation of a decimal number that is an offset into the string table.
    /// Executable images do not use a string table and do not support section names longer than 8 characters.
    /// Long names in object files are truncated if they are emitted to an executable file.
    char name[8];
    /// The total size of the section when loaded into memory.
    /// If this value is greater than SizeOfRawData, the section is zero-padded.
    /// This field is valid only for executable images and should be set to zero for object files.
    uint32_t virtual_size;
    /// For executable images, the address of the first byte of the section relative to the image base
    /// when the section is loaded into memory. For object files,
    /// this field is the address of the first byte before relocation is applied;
    /// for simplicity, compilers should set this to zero.
    /// Otherwise, it is an arbitrary value that is subtracted from offsets during relocation.
    uint32_t virtual_address;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files).
    /// For executable images, this must be a multiple of FileAlignment from the optional header.
    /// If this is less than VirtualSize, the remainder of the section is zero-filled.
    /// Because the SizeOfRawData field is rounded but the VirtualSize field is not,
    /// it is possible for SizeOfRawData to be greater than VirtualSize as well.
    /// When a section contains only uninitialized data, this field should be zero.
    uint32_t size_of_raw_data;
    /// The file pointer to the first page of the section within the COFF file.
    /// For executable images, this must be a multiple of FileAlignment from the optional header.
    /// For object files, the value should be aligned on a 4-byte boundary for best performance.
    /// When a section contains only uninitialized data, this field should be zero.
    uint32_t pointer_to_raw_data;
    /// The file pointer to the beginning of relocation entries for the section.
    /// This is set to zero for executable images or if there are no relocations.
    uint32_t pointer_to_relocations;
    /// The file pointer to the beginning of line-number entries for the section.
    /// This is set to zero if there are no COFF line numbers.
    /// This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t pointer_to_linenumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t number_of_relocations;
    /// The number of line-number entries for the section.
    /// This value should be zero for an image because COFF debugging information is deprecated.
    uint16_t number_of_linenumbers;
    /// The flags that describe the characteristics of the section.
    uint32_t characteristics;
};

#ifdef _MSC_VER
#pragma packed(pop)
#endif

/// Structure containing PE file data
struct PEFile {
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Optional header (mocked)
    char optional_header[240];
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;
    ///@}
};

/// @brief Writes a PE file's section to given file by its name
/// @param[in] pe_file A representation of a PE file as a struct
/// @param[in] name Name of a section that shall be written
/// @param[in] source File from which the section of a PE file by the given name shall be read
/// @param[in] output File to which the section by the given name will be written in case of success
/// @return true if the section was successfully written to a given file, false otherwise
bool write_section_by_name(const struct PEFile *pe_file, const char *name, FILE *source, FILE *output);

/// @brief Reads the contents of a PE file to a struct representing that data
/// @param[in] source File from which the PE file shall be read
/// @param[in] pe_file A representation of a PE file as a struct returned by that function
/// @return true if reading the given PE file succeeded, false otherwise
bool read(FILE *source, struct PEFile *pe_file);

#endif //SECTION_EXTRACTOR_PE_H
