/// @file 
/// @brief Main application file

#include "pe.h"
#include <stdio.h>
#include <stdlib.h>


/// @brief A subroutine that exits the program with the given code. All given files are closed, memory is freed.
void clean_and_exit(int exit_code, char *message, FILE *to_close_1, FILE *to_close_2, struct PEFile *to_free) {
    fclose(to_close_1);
    fclose(to_close_2);
    if (to_free != NULL)
        free(to_free->section_headers);
    if (message != NULL)
        fprintf((exit_code == 0) ? stdout : stderr, "%s", message);
    exit(exit_code);
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv) {
    const int arguments_expected = 4;
    if (argc != arguments_expected)
        clean_and_exit(EXIT_FAILURE, "Wrong number of arguments", NULL, NULL, NULL);
    const char *source_file_name = argv[1];
    const char *section_name = argv[2];
    const char *output_file_name = argv[3];

    FILE *source = fopen(source_file_name, "rb");
    FILE *output = fopen(output_file_name, "wb");
    if (!source || !output)
        clean_and_exit(EXIT_FAILURE, "Cannot open file(s)", source, output, NULL);

    struct PEFile pe_file;
    if (!read(source, &pe_file))
        clean_and_exit(EXIT_FAILURE, "Cannot read PE file", source, output, &pe_file);
    if (!write_section_by_name(&pe_file, section_name, source, output))
        clean_and_exit(EXIT_FAILURE, "Cannot write section by given name to output file",
                       source, output, &pe_file);

    clean_and_exit(EXIT_SUCCESS, "OK", source, output, &pe_file);

    return 0;
}
