/// @file
/// @brief File that contains io operations on PE files

#include "../include/pe.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


/// @brief Reads the contents of a PE file to a struct representing that data
/// @param[in] source File from which the PE file shall be read
/// @param[in] pe_file A representation of a PE file as a struct returned by that function
/// @return true if reading the given PE file succeeded, false otherwise
bool read(FILE *source, struct PEFile *pe_file) {
    if (!source) return false;

    // read and apply magic offset
    if (fseek(source, OFFSET_TO_PE_SIGNATURE, SEEK_SET)) return false;
    if (!fread(&pe_file->magic_offset, sizeof(pe_file->magic_offset), 1, source)) return false;
    if (fseek(source, pe_file->magic_offset, SEEK_SET)) return false;

    // read and compare PE magic value
    if (!fread(&pe_file->magic, sizeof(pe_file->magic), 1, source)) return false;
    if (pe_file->magic != MAGIC_VALUE) return false;

    // read PE header
    if (!fread(&pe_file->header, sizeof(pe_file->header), 1, source)) return false;

    // read optional header
    if (!fread(&pe_file->optional_header, pe_file->header.size_of_optional_header, 1, source)) return false;

    // read section headers
    pe_file->section_headers = malloc(pe_file->header.number_of_sections * sizeof(struct SectionHeader));
    if (!pe_file->section_headers) return 0;
    for (size_t i = 0; i < pe_file->header.number_of_sections; i++)
        if (!fread(&pe_file->section_headers[i], sizeof(struct SectionHeader), 1, source)) return 0;
    return true;
}


/// @brief Writes a PE file's section to given file with the given section header
/// @param[in] section_header The header of a section that shall be written to the given file
/// @param[in] source File from which the section of a PE file with the given header shall be read
/// @param[in] output File to which the section with the given  header will be written in case of success
/// @return true if the section was successfully written to a given file, false otherwise
static inline bool write(const struct SectionHeader *section_header, FILE *source, FILE *output) {
    if (!source || !output) return false;
    if (fseek(source, section_header->pointer_to_raw_data, SEEK_SET)) return false;

    char raw_data[section_header->size_of_raw_data];
    if (!fread(raw_data, section_header->size_of_raw_data, 1, source)) return false;
    if (!fwrite(raw_data, section_header->size_of_raw_data, 1, output)) return false;
    return true;
}

/// @brief Writes a PE file's section to given file by its name
/// @param[in] pe_file A representation of a PE file as a struct
/// @param[in] name Name of a section that shall be written
/// @param[in] source File from which the section of a PE file by the given name shall be read
/// @param[in] output File to which the section by the given name will be written in case of success
/// @return true if the section was successfully written to a given file, false otherwise
bool write_section_by_name(const struct PEFile *pe_file, const char *name, FILE *source, FILE *output) {
    for (size_t i = 0; i < pe_file->header.number_of_sections; i++)
        if (strcmp(name, pe_file->section_headers[i].name) == 0)
            return write(&pe_file->section_headers[i], source, output);
    return false;
}
